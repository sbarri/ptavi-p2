
import math
import sys


class Compute():
    import computeoochild
    count = 0

    def __init__(self):
        self.default = 2

    def power(self, num, exp=2):

        self.count += 1
        return num ** exp

    def log(self, num, base=2):

        self.count += 1
        return math.log(num, base)

    def set_def(self, num2):
        self.count += 1
        if num2 is None:
            return self.default
        else:
            if num2 <= 0:
                raise ValueError
            else:
                return num2

    def get_def(self):
        self.count += 1
        return self.default