

import math
import sys

class Compute ():

    def __init__(self, default=2):

        self.default = default

    def selfdef(self):
        return self

    def power(num, self):

        return num ** self

    def log(num, self):

        return math.log(num, self)


if __name__ == "__main__":
    if len(sys.argv) < 3:
        sys.exit()

    try:
        num = float(sys.argv[2])
    except ValueError:
        sys.exit()

    if len(sys.argv) == 3:
        num2 = Compute().default
    else:
        try:
            num2 = float(sys.argv[3])
        except ValueError:
            sys.exit()

    if sys.argv[1] == "power":
        result = Compute.power(num, num2)
    elif sys.argv[1] == "log":
        result = Compute.log(num, num2)
    else:
        sys.exit()

    print(result)


